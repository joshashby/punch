(ns punch.core
  (:gen-class)
  (:use [seesaw core keystroke table]
        [cupboard.utils])
  (:require [seesaw.bind :as b]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clj-time.local :as l]
            [clj-time.coerce :as c]
            [clj-time.periodic :as p]
            [clj-pdf.core :as pdf]
            [cupboard.core :as cb]
            [clojure.java.io :as io]
            [clj-yaml.core :as yaml]))


;=============================================================================
; Util Things!
;=============================================================================
(def day-formatter (f/formatter-local "EE d-M-YY"))
(def time-formatter (f/formatter-local "h:mm a"))

(defn uuid [] (str (java.util.UUID/randomUUID)))

(defn get-table-event [e]
  (select (to-root (-> e .getSource)) [:#table]))

(def total-hours (atom ""))
(def button-wording (atom ""))

(def settings (yaml/parse-string (slurp "settings.yaml")))


;=============================================================================
; Database Things!
;=============================================================================
(cb/defpersist punch
   ((:id :index :unique)
   (:day :index :any)
   (:in)
   (:out)))

(defn open-database []
  (cb/open-cupboard! "punch.db"))

(defn get-punches []
  (cb/query))


;=============================================================================
; Timecard Things!
;=============================================================================
(defn punch-card []
  (let [now (l/local-now)
        today (t/today)]

    (try
      (def day-entries (cb/query (date= :day today)))
      (def day (last (sort-by :in day-entries)))
      (if-not (day :out)
        (cb/passoc! day :out now)
        (cb/make-instance punch
                          [(uuid) (t/today) now nil]))

    (catch Exception e
      (cb/make-instance punch
                        [(uuid) (t/today) now nil])))))

(defn calc-total-for-day [date]
  (try
    (def day-entries (cb/query (date= :day date)))
    (reduce + (for [entry day-entries]
                (if (entry :out)
                    (t/in-millis (t/interval (entry :in) (entry :out)))
                    0)))

    (catch Exception e 0)))

(defn millis-to-hours-minutes [millis]
  (def hours (quot millis 3600000))
  (def minutes (quot (- millis (* hours 3600000)) 60000))
  {:hours hours :minutes minutes})

(defn biweekly-millis []
  (let [days (take 15 (p/periodic-seq (-> 2 t/weeks t/ago) (t/days 1)))]

    (millis-to-hours-minutes (reduce + (for [day days]
                                         (calc-total-for-day (c/to-local-date day)))))))

(defn update-total-hours []
  (let [timez (biweekly-millis)]

    (reset! total-hours
            (format "%d:%d Hours since %s (14 days ago)"
                    (timez :hours)
                    (timez :minutes)
                    (f/unparse day-formatter (-> 2 t/weeks t/ago))))))

(defn update-button-wording []
  (let [today (t/today)]

    (try
      (def day-entries (cb/query (date= :day today)))
      (def day (last (sort-by :in day-entries)))
      (if-not (day :out)
        (reset! button-wording "Punch Out")
        (reset! button-wording "Punch In"))

    (catch Exception e
      (reset! button-wording "Punch In")))))

; This could be updated to do fancy things and only update the one row thats
; needed, *but*... I'm just too damn lazy right now
(defn update-table [tbl]
  (clear! tbl)
  (def entries (sort-by :in (get-punches)))
  (doseq [entry entries]
    (def day (f/unparse day-formatter
                          (c/to-date-time (entry :day))))

    ; If this fails, then something is wrong with our data because :in should
    ; always have a timestamp value
    (def in (f/unparse time-formatter (c/to-date-time (entry :in))))

    (def out (if (or (not (entry :out)) (= (entry :out) 0))
               "Not clocked out"
               (f/unparse time-formatter (c/to-date-time (entry :out)))))

    (insert-at! tbl 0 { :id 0
                        :day day
                        :in in
                        :out out })))

(defn update [tbl]
  (do (update-table tbl)
      (update-total-hours)
      (update-button-wording)))

(defn punch-card-from-button [e]
  (let [tbl (get-table-event e)]

    (punch-card)
    (update tbl)))


;=============================================================================
; PDF Invoice Things!
;=============================================================================

(defn generate-pdf-invoice []
  (let [date (f/unparse day-formatter (t/now))
        filename (clojure.string/replace (format "Invoice_%s_%s.pdf"
                                                 (settings :name) date)
                                         #" " "_")
        number 2]

    (pdf/pdf [{:title (format "Invoice %s" date)
               :creator "Punch!"
               :author "Joshua Ashby"
               :right-margin 25
               :left-margin 25
               :top-margin 25
               :bottom-margin 25
               :size :letter
               :orientation :portrait
               :font {:size 10}
               :pages false
               :footer false}

              [:heading "Invoice Payment"]

              [:table {:header [{:color [100 100 100]} "Date" "Invoice Num"]
                       :width 40
                       :align :center}

               [[:chunk date] [:chunk number]]]

              [:table {:header [{:color [100 100 100]} "From" "To"]
                       :width 100
                       :num-cols 2}

               [[:paragraph (settings :name) "\n" (settings :address)] [:chunk (settings :bill-to)]]]

              [:spacer]

              [:table {:header [{:color [100 100 100]} "Week(s)" "Hours" "Rate" "Week Total"]
                       :num-cols 4
                       :width 100
                       :widths [3 1 1 1]}

               ["Internship week $DATE" "40" "20" "800"]]

              [:spacer]

              [:table {:align :right
                       :width 30}
               [[:cell {:color [100 100 100]
                        :align :right} "Period Total"] "800"]]

              [:spacer 2]

              [:table {:border-width 0
                       :cell-border false
                       :num-cols 2
                       :align :center
                       :width 75
                       :widths [1 2]}
               ["Employee Signature" "__________________________________________"]]

              [:spacer]

              [:table {:border-width 0
                       :cell-border false
                       :num-cols 2
                       :align :center
                       :width 75
                       :widths [1 2]}
               ["Manager Signature" "__________________________________________"]]

             ] filename)))


;=============================================================================
; GUI Things!
;=============================================================================
; Make things look pretty
(native!)

; Dialogs
(defn confirm-clear-timesheet [e]
  (-> (dialog :content "Are you sure you want to clear the timesheet?"
              :option-type :ok-cancel
              :success-fn (fn [a] (cb/clear-shelf)
                                  (update (get-table-event e))))
      pack!
      show!))

; Menus
(def file-menu
  (let [exit-menu-item (action  :name "Exit" :tip "Exit the application"
                                :mnemonic \q :key (keystroke "ctrl Q")
                                :handler (fn [e]
                                           (System/exit 0)))

        create-invoice (action  :name "Invoice PDF" :tip "Generate a PDF of the invoice for the last two weeks"
                                :mnemonic \c :key (keystroke "ctrl S")
                                :handler (fn [e] (generate-pdf-invoice)))
        export-to-csv (action   :name "Export to CSV" :tip "Export the database to CSV for importing into xecel or similar."
                                :handler (fn [e]
                                           ()))
        import-from-csv (action :name "Import from CSV" :tip "Import a database from CSV"
                                :handler (fn [e]
                                           ()))]

    (menu :text "File"
          :mnemonic \f
          :items [create-invoice export-to-csv import-from-csv exit-menu-item])))

(def edit-menu
  (let [clear-menu-item (action :name "Clear" :tip "Clear the timesheet"
                                :handler confirm-clear-timesheet)]

    (menu :text "Edit"
          :mnemonic \e
          :items [clear-menu-item])))

; Menubar
(def menu-bar
  (menubar :items [file-menu edit-menu]))

; Main toolbar with things in it; We'll use a panel rather than a toolbar just
; because looks
(def main-toolbar
  (let [in-out-btn (button :id :in-out)
        current-hours-label (label :id :current-hours)]

    (b/bind total-hours current-hours-label)
    (b/bind button-wording (b/property in-out-btn :text))

    (listen in-out-btn :action punch-card-from-button)

    (horizontal-panel :items [in-out-btn current-hours-label])))

; Where we'll display the timesheet data...
(def table-frame
  (let [tbl (table :id :table
                     :model [
                      :columns [ { :key :day :text "Day" }
                                 { :key :in :text "In" }
                                 { :key :out :text "Out" } ]]
                   :show-grid? true)]

    (scrollable tbl :column-header ["Day" "In" "Out"])))

; And something to hold it all
(def window-content
  (border-panel :north main-toolbar
                :center table-frame))

; Main frame which is used for the application
(def window-frame
  (frame :title "Punch!"
         :size [500 :by 300]
         :on-close :exit
         :content window-content
         :menubar menu-bar))


;=============================================================================
; Other Things! (and main)
;=============================================================================
(defn bootstrap [root]
  (let [tbl (select root [:#table])]
    (if (get-punches)
      (update tbl)))
  root)

; Display things!
(defn -main [& args]
  (open-database)

  (-> window-frame
      bootstrap
      show!))
