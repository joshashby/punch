(defproject punch "0.1.0-SNAPSHOT"
  :description "Basic little timesheet program for fun"
  :url ""
  :license {:name "MIT"
            :url ""}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [seesaw "1.4.2"]
                 [cupboard "1.0beta1"]
                 [clj-time "0.7.0"]
                 [clj-pdf "1.11.17"]
                 [clj-yaml "0.4.0"]]
  :resource-paths ["resources"]
  :main ^:skip-aot punch.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
